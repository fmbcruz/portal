<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use App\Comment;
use App\Option;
use App\Page;
use App\Post;
use App\Statistic;
use Validator;


class BlogController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home(Request $request)
    {
        $collection                        = Page::where('slug', 'blog')->select('content as highlight')->first();
        $collection->highlights            = json_decode($collection->highlight);
        unset($collection->highlight);
        $collection['menu_blog_main']        = Option::where('name', 'menu_blog_main')->first();
        $collection['redes_sociais_blog']    = Option::where('name', 'redes_sociais_blog')->first();
        $collection['posts']                 = Post::where('status', 'published')->orderBy('created_at', 'desc')->paginate($collection->highlights->page_size);
        $collection['popular_posts']         = Post::where('status', 'published')->orderBy('views', 'desc')->limit(6)->get();


        foreach ($collection->highlights->highlight_blog as $key => $id) {
            $collection->highlights->$key = self::postInfo($id);
        }

        self::setAnalytics($request->ip());

        return view('blog.layouts.home', compact('collection'));
    }

    /**
     * @param $slug
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPost($slug, Request $request)
    {
        $collection                          = Post::where('slug', $slug)->first();
        $collection->increment('views');
        $collection['menu_blog_main']        = Option::where('name', 'menu_blog_main')->first();
        $collection['redes_sociais_blog']    = Option::where('name', 'redes_sociais_blog')->first();
        $template                            = 'blog.layouts.' . $collection->type;
        $relatedPosts                        = Post::postPrevAndNext($collection->created_at);
        $collection['popular_posts']         = Post::where('status', 'published')->orderBy('views', 'desc')->limit(6)->get();

        self::setAnalytics($request->ip());

        if(count($collection) > 0 && ($collection->status == 'published' || Auth::check())) {
            return view($template, compact('collection','relatedPosts'));
        } else {
            return view('404');
        }
    }

    /**
     * @param $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function category($category)
    {
        $collection['posts'] = Post::where('status', 'published')
            ->whereHas('categories', function ($query) use ($category) {
                $query->where('slug', $category);
            })
            ->orderBy('created_at', 'desc')
            ->paginate(12);

        $collection['term']                = $category;
        $collection['menu_blog_main']      = Option::where('name', 'menu_blog_main')->first();
        $collection['redes_sociais_blog']  = Option::where('name', 'redes_sociais_blog')->first();
        $collection['popular_posts']       = Post::where('status', 'published')->orderBy('views', 'desc')->limit(6)->get();

        return view('blog.layouts.category', compact('collection'));
    }

    public function tag($tag)
    {
        $collection['posts'] = Post::where('status', 'published')
            ->whereHas('tags', function ($query) use ($tag) {
                $query->where('slug', $tag);
            })
            ->orderBy('created_at', 'desc')
            ->paginate(12);

        $collection['term']                = $tag;
        $collection['menu_blog_main']      = Option::where('name', 'menu_blog_main')->first();
        $collection['redes_sociais_blog']  = Option::where('name', 'redes_sociais_blog')->first();
        $collection['popular_posts']       = Post::where('status', 'published')->orderBy('views', 'desc')->limit(6)->get();

        return view('blog.layouts.tag', compact('collection'));
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function comment(Request $request)
    {

        $data = $request->all();
        $data['author_ip'] = $request->ip();

        $rules = [
            'author' => 'required|string|max:255',
            'email'  => 'required|string|email|max:255',
            'content'=> 'required'
        ];

        $messages = [
            'author.required' => 'author-required',
            'author.max'    => 'author-max',
            'email.required' => 'email-required',
            'email.email' => 'email-email',
            'email.max'    => 'email-max',
            'content.required' => 'content-required',
        ];

        $validator = Validator::make($data, $rules, $messages);

        if($validator->fails()) {
            return Redirect::to(URL::previous() . "#comentario")->withErrors($validator)->withInput();
        }

        Comment::create($data);

        return Redirect::to(URL::previous() . "#comentario")->with('status', 'obrigado pelo comentário! Em breve ele estará disponível.');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function busca(Request $request)
    {
        $collection['menu_blog_main']      = Option::where('name', 'menu_blog_main')->first();
        $collection['redes_sociais_blog']  = Option::where('name', 'redes_sociais_blog')->first();
        $collection['posts']               = Post::searchBlog($request->termo);
        $collection['term']                = $request->termo;
        $collection['popular_posts']       = Post::where('status', 'published')->orderBy('views', 'desc')->limit(6)->get();

        return view('blog.layouts.search', compact('collection'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contato(Request $request)
    {
        $collection['menu_blog_main']        = Option::where('name', 'menu_blog_main')->first();
        $collection['redes_sociais_blog']    = Option::where('name', 'redes_sociais_blog')->first();

        self::setAnalytics($request->ip());

        return view('blog.layouts.contact', compact('collection'));
    }

    /**
     * @param null $id
     * @return mixed
     */
    private static function postInfo($id=null)
    {
        $post = Post::find($id);
        $post['categories'] = $post->categories;
        $post['author']     = $post->author;

        return $post;

    }

    /**
     * @param $ip
     */
    public function setAnalytics($ip)
    {
        $url = 'http://ip-api.com/json/' . $ip;

        $result = file_get_contents($url);
        $result = json_decode($result, true);
        $result['url'] = url()->current();

        Statistic::create($result);

    }
}
