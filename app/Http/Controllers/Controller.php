<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getChart($width=400, $height=200, $chart='doughnut', $data=[], $title=null,$options=null)
    {

        $titleDisplay =  $title ? 'true' : 'false';

        $dataset = [
            'backgroundColor' => $data['bg'],
            'hoverBackgroundColor' => $data['bgHover'],
            'data' => $data['data']
        ];

        if($chart === 'line')
            $dataset = [
                "label" => $title,
                'backgroundColor' => "rgba(153, 102, 255, 0.31)",
                'borderColor' => "rgba(153, 102, 255, 0.7)",
                "pointBorderColor" => "rgba(153, 102, 255, 0.7)",
                "pointBackgroundColor" => "rgba(153, 102, 255, 0.7)",
                "pointHoverBackgroundColor" => "rgba(153, 102, 255, 0.7)",
                "pointHoverBorderColor" => "rgba(220,220,220,1)",
                'data' => $data['data'],
            ];

        $chart = app()->chartjs
            ->name($chart)
            ->type($chart)
            ->size(['width' => $width, 'height' => $height])
            ->labels($data['label'])
            ->datasets([$dataset]);
        if(empty($options))
            return $chart->options([]);
        if($options == 'bar')
            return $chart->optionsRaw("{
            responsive: true,
            legend: {
                display:false
            },
            scales: {
                xAxes: [{
                    gridLines: {
                        display:false
                    }  
                }]
            },
            tooltips: {
              displayColors: false
            },
            title: {
                display: " . $titleDisplay . ",
                text: '" . $title . "'
            }
        }");
        if($options == 'pie')
            return $chart->optionsRaw("{
            responsive: true,
            legend: {
                display:false
            },
            tooltips: {
              displayColors: false
            },
            title: {
                display: " . $titleDisplay . ",
                text: '" . $title . "'
            }
        }");
    }

    public function formatData($totals)
    {
        $data['label'] = [];
        $data['data'] = [];
        $data['bg'] = [];
        $data['bgHover'] = [];

        foreach ($totals as $key => $value) {
            $color = $this->generateColor($key);
            array_push($data['label'] ,$value->label);
            array_push($data['bg'] ,$color);
            array_push($data['bgHover'] ,$color . 'b8');
            array_push($data['data'] ,$value->total);
        }

        return $data;
    }

    public function generateColor(&$n)
    {
        $colors = ['ff6384', 'ff9f40', '96f', '36a2eb', 'ffcd56', '4bc0c0', 'c9cbcf'];
        if($n >= count($colors))$n = 0;
        return '#'.$colors[$n];
    }
}
