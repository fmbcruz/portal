<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Category;
use App\Tag;
use Illuminate\Support\Facades\DB;
use Validator;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
            ['title' => 'Artigos', 'icon' => 'files-o'],
        ]);

        $search = $request->busca;

        if(empty($search)) {
            $collection = Post::getPosts(10);
        } else {
            $collection = Post::search($search);
        }
        
        return view('admin.post.index', compact('collection', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
            ['title' => 'Artigos', 'url' => route('artigos'), 'icon' => 'files-o'],
            ['title' => 'Novo Artigo', 'url' => '', 'icon' => 'file-o'],
        ]);

        $posts = Post::all();
        $categories = Category::whereNull('parent_id')->get();

        return view('admin.post.create', compact('breadcrumbs', 'posts', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       return self::verifyData($request->all(), null);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
            ['title' => 'Artigos', 'url' => route('artigos'), 'icon' => 'files-o'],
            ['title' => 'Editar Artigo', 'url' => '', 'icon' => 'file-o'],
        ]);
        $collection = Post::find($id);
        $posts = Post::all();
        $categories = Category::whereNull('parent_id')->get();

        return view('admin.post.edit', compact('collection','posts','breadcrumbs', 'categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return self::verifyData($request->all(), $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::deletePost($id);

        return redirect()->back();
    }

    private static function verifyData($data, $id)
    {
        $date = '';

        $data['slug'] = str_slug($data['slug']);

        if(!empty($data['date'])):
            $data['status'] = $data['date'];
            $data['date']   = self::datePublish($data);
            if(strtotime($data['date']) < time()):
                $date = 'required';
                $data['date'] = null;
            endif;
        else:
            $data['date'] = null;
        endif;

        if(empty($data['type'])):
            $data['type'] = 'default';
        endif;

        if(!empty($data['imagem'])):
            $data['image'] = $data['imagem'];
        endif;

        if(is_array($data['content'])):
            $data['content'] = json_encode($data['content']);
        endif;

        $unique = '';
        if(!empty($id)) {
            $unique = ',slug,'.$id;
        }

        $rules = [
            'title'      => 'required|max:100',
            'slug'       => 'required|unique:posts'.$unique,
            'date'       => $date,
        ];

        $messages = [
            'title.required'      => 'O campo título é obrigatório',
            'slug.unique'         => 'O slug já existe',
            'slug.required'       => 'O slug é obrigatório',
            'date.required'       => 'Data de agendamento inválida.',
        ];

        $validator = Validator::make($data, $rules, $messages);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if(empty($data['categories'])):
            $data['categories'][15] = 'on';
        endif;

        if(empty($id)):
            $post = Post::create($data);

            // Relationship with categories
            self::categoriesPost($data['categories'], $post);

            // Relationship with tags
            self::tagsPost($data['tags'], $post);

            return redirect()->route('artigos');
        else:
            $post = Post::find($id);

            $post->update($data);

            // Relationship with categories
            self::categoriesPost($data['categories'], $post);

            // Relationship with tags
            self::tagsPost($data['tags'], $post);

            return redirect()->back()->withErrors(['message', 'Artigo atualizada!']);
        endif;

    }

    private static function datePublish($data)
    {
        return $data['aa'] . '-' . $data['mm'] . '-' . $data['dd'] . ' ' . $data['hh'] . ':' . $data['mn'] . ':' . '00';
    }

    private static function categoriesPost($categories, $post)
    {
        $categoriesPost = [];
        foreach ($categories as $key => $val) {
            array_push($categoriesPost, $key);
        }

        $post->categories()->sync($categoriesPost);
    }

    private static function tagsPost($tags, $post=null) {
        $tagsPost = json_decode($tags);

        $syncTags = [];

        foreach ($tagsPost as $key => $tag) {
            $tags = Tag::where('name', '=', $tag)->first();

            if($tags) {
                array_push($syncTags, $tags->id);
            } else {
                $newTag = DB::table('tags')->insertGetId(['name' => $tag, 'slug' => str_slug($tag)]);
                array_push($syncTags, $newTag);
            }
        }

        $post->tags()->sync($syncTags);

    }

    // API

    public static function posts()
    {
        return Post::where('status', 'published')
                ->orderBy('created_at', 'desc')
                ->take(30)
                ->get();

    }

    public static function postInfo($id=null)
    {
        $post = Post::find($id);
        $post['categories'] = $post->categories;
        $post['author']     = $post->author;

        return $post;

    }

}
