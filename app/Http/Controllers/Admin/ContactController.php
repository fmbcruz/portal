<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
            ['title' => 'Contatos', 'icon' => 'envelope-o'],
        ]);

        $search = $request->busca;

        if(empty($search)) {
            $collection = Contact::getContacts(10);
        } else {
            $collection = Contact::search($search);
        }

        return view('admin.contact.index', compact('collection', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
            ['title' => 'Comentários', 'url' => route('comentarios'), 'icon' => 'comments-o'],
            ['title' => 'Editar Comentário', 'url' => '', 'icon' => 'comment'],
        ]);

        $collection = Contact::getContact($id);

        return $collection;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Contact::find($id)->delete();

        return redirect()->back();
    }

}
