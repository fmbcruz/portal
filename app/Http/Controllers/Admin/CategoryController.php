<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
            ['title' => 'Categorias', 'icon' => 'bookmark'],
        ]);

        $search = $request->busca;

        if(empty($search)) {
            $collection = Category::getCategories(10);
        } else {
            $collection = Category::search($search);
        }

        $categories = Category::whereNull('parent_id')->get();

        return view('admin.category.index', compact('collection', 'breadcrumbs', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
            ['title' => 'Categorias', 'url' => route('categorias'), 'icon' => 'bookmark'],
            ['title' => 'Nova Categoria', 'url' => '', 'icon' => 'bookmark-o'],
        ]);

        $categories = Category::all();

        return view('admin.category.create', compact('breadcrumbs', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       return self::verifyData($request->all(), null);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
            ['title' => 'Categorias', 'url' => route('categorias'), 'icon' => 'bookmark'],
            ['title' => 'Editar Tag', 'url' => '', 'icon' => 'bookmark-o'],
        ]);
        $collection = Category::find($id);
        $categories = Category::whereNull('parent_id')->get();

        return view('admin.category.edit', compact('collection','categories','breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return self::verifyData($request->all(), $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::find($id)->delete();

        return redirect()->back();
    }

    private static function verifyData($data, $id)
    {
        $unique = '';
        if(!empty($id)) {
            $unique = ',name,'.$id;
        }

        $rules = [
            'name' => 'required|max:100|unique:categories' . $unique,
            'slug'  => 'required',
        ];

        $messages = [
            'name.required' => 'O campo nome é obrigatório',
            'name.unique'    => 'Esta categoria já existe',
            'slug.required'    => 'O slug é obrigatório',
        ];

        $validator = Validator::make($data, $rules, $messages);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $data['slug'] = str_slug($data['slug']);

        if(empty($id)):
            Category::create($data);
        else:
            Category::find($id)->update($data);
        endif;

        return redirect()->route('categorias');

    }

}
