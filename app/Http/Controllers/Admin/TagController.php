<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tag;
use Validator;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
            ['title' => 'Tags', 'icon' => 'tags'],
        ]);

        $search = $request->busca;

        if(empty($search)) {
            $collection = Tag::getTags(10);
        } else {
            $collection = Tag::search($search);
        }



        return view('admin.tag.index', compact('collection', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
            ['title' => 'Tags', 'url' => route('tags'), 'icon' => 'tags'],
            ['title' => 'Nova Tag', 'url' => '', 'icon' => 'tags'],
        ]);

        $tags = Tag::all();

        return view('admin.tag.create', compact('breadcrumbs', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       return self::verifyData($request->all(), null);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
            ['title' => 'Tags', 'url' => route('tags'), 'icon' => 'tags'],
            ['title' => 'Editar Tag', 'url' => '', 'icon' => 'tags'],
        ]);
        $collection = Tag::find($id);
        $tags = Tag::all();

        return view('admin.tag.edit', compact('collection','tags','breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return self::verifyData($request->all(), $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tag::find($id)->delete();

        return redirect()->back();
    }

    private static function verifyData($data, $id)
    {
        $unique = '';
        if(!empty($id)) {
            $unique = ',name,'.$id;
        }

        $rules = [
            'name' => 'required|max:100|unique:tags' . $unique,
            'slug'  => 'required',
        ];

        $messages = [
            'name.required' => 'O campo nome é obrigatório',
            'name.unique'    => 'Esta tag já existe',
            'slug.required'    => 'O slug é obrigatório',
        ];

        $validator = Validator::make($data, $rules, $messages);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $data['slug'] = str_slug($data['slug']);

        if(empty($id)):
            Tag::create($data);
        else:
            Tag::find($id)->update($data);
        endif;

        return redirect()->route('tags');

    }

    public function tagsName()
    {
        $tags = Tag::getTagsNames();
        $labelTags = [];
        foreach ($tags as $key => $tag) {
            array_push($labelTags, $tag->name);
        }
        sort($labelTags);
        return $labelTags;
    }

}
