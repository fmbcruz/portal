<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
            ['title' => 'Usuários', 'icon' => 'users'],
        ]);

        if(empty($search)) {
            $collection = User::getUsers(10);
        } else {
            $collection = User::search($search);
        }

        return view('admin.user.index', compact('breadcrumbs', 'collection'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
            ['title' => 'Usuários', 'url' => route('usuarios'), 'icon' => 'users'],
            ['title' => 'Novo Usuário', 'url' => '', 'icon' => 'user'],
        ]);

        $users = User::all();

        return view('admin.post.create', compact('breadcrumbs', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required|string|max:255',
            'email'  => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6'
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $data['password'] = bcrypt($data['password']);


        User::create($data);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
            ['title' => 'Usuários', 'url' => route('usuarios'), 'icon' => 'users'],
            ['title' => 'Editar Usuário', 'url' => '', 'icon' => 'user'],
        ]);

        $collection = User::find($id);

        return view('admin.user.edit', compact('collection','breadcrumbs'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        if(isset($data['password']) && !empty($data['password'])):
            $validator = Validator::make($data, [
                'name' => 'required|string|max:255',
                'email'  => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($id)],
                'password' => 'required|string|min:6'
            ]);
            $data['password'] = bcrypt($data['password']);
        else:
            $validator = Validator::make($data, [
                'name' => 'required|string|max:255',
                'email'  => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($id)],
            ]);
            unset($data['password']);
        endif;

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        User::find($id)->update($data);

        return redirect()->back()->withErrors(['message', 'Usuário atualizada!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->back();
    }
}
