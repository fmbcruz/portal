<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;
use Validator;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
            ['title' => 'Páginas', 'icon' => 'files-o'],
        ]);

        $search = $request->busca;

        if(empty($search)) {
            $collection = Page::getPages(10);
        } else {
            $collection = Page::search($search);
        }



        return view('admin.page.index', compact('collection', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
            ['title' => 'Páginas', 'url' => route('paginas'), 'icon' => 'files-o'],
            ['title' => 'Nova Página', 'url' => '', 'icon' => 'file-o'],
        ]);

        $pages = Page::all();

        return view('admin.page.create', compact('breadcrumbs', 'pages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       return self::verifyData($request->all(), null);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
            ['title' => 'Páginas', 'url' => route('paginas'), 'icon' => 'files-o'],
            ['title' => 'Editar Página', 'url' => '', 'icon' => 'file-o'],
        ]);
        $collection = Page::find($id);
        $pages = Page::all();

        return view('admin.page.edit', compact('collection','pages','breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return self::verifyData($request->all(), $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Page::find($id)->delete();

        return redirect()->back();
    }

    private static function verifyData($data, $id)
    {
        $date = '';
        $data['slug'] = str_slug($data['slug']);
        if(!empty($data['date'])):
            $data['status'] = $data['date'];
            $data['date']   = self::datePublish($data);
            if(strtotime($data['date']) < time()):
                $date = 'required';
                $data['date'] = null;
            endif;
        else:
            $data['date'] = null;
        endif;

        if(empty($data['type'])):
            $data['type'] = 'default';
        endif;

        if(!empty($data['imagem'])):
            $data['image'] = $data['imagem'];
        endif;

        if(is_array($data['content'])):
            $data['content'] = json_encode($data['content']);
        endif;

        $unique = '';
        if(!empty($id)) {
            $unique = ',slug,'.$id;
        }

        $rules = [
            'title' => 'required|max:100',
            'slug'  => 'required|unique:pages'.$unique,
            'date'  => $date
        ];

        $messages = [
            'title.required' => 'O campo título é obrigatório',
            'slug.unique'    => 'O slug já existe',
            'slug.required'    => 'O slug é obrigatório',
            'date.required'   => 'Data inválida de agendamento.',
        ];

        $validator = Validator::make($data, $rules, $messages);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if(empty($id)):
            Page::create($data);
            return redirect()->route('paginas');
        else:
            Page::find($id)->update($data);
            return redirect()->back()->withErrors(['message', 'Página atualizada!']);
        endif;

    }

    private static function datePublish($data)
    {
        return $data['aa'] . '-' . $data['mm'] . '-' . $data['dd'] . ' ' . $data['hh'] . ':' . $data['mn'] . ':' . '00';
    }

}
