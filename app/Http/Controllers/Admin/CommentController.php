<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;
use Validator;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
            ['title' => 'Comentários', 'icon' => 'comments-o'],
        ]);

        $search = $request->busca;

        if(empty($search)) {
            $collection = Comment::getComments(10);
        } else {
            $collection = Comment::search($search);
        }



        return view('admin.comment.index', compact('collection', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $breadcrumbs = json_encode([
            ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
            ['title' => 'Comentários', 'url' => route('comentarios'), 'icon' => 'comments-o'],
            ['title' => 'Editar Comentário', 'url' => '', 'icon' => 'comment'],
        ]);
        $collection = Comment::find($id);

        return view('admin.comment.edit', compact('collection','breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $rules = [
            'author'    => 'required',
            'email'     => 'required',
            'content'   => 'required',
        ];

        $messages = [
            'author.required'   => 'O campo nome é obrigatório',
            'email.required'    => 'O campo email é obrigatório',
            'content.required'  => 'O comentário é obrigatório',
        ];

        $validator = Validator::make($data, $rules, $messages);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        Comment::find($id)->update($data);

        return redirect()->route('comentarios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Comment::find($id)->delete();

        return redirect()->back();
    }

}
