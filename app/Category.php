<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{

    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name','slug', 'parent_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public static function getCategories($page_size)
    {
        return DB::table('categories')
            ->select('id', 'name', 'slug')
            ->paginate($page_size);
    }

    public static function search($term)
    {
        return DB::table('categories')
            ->where('name', 'like', "%". strtolower($term) . "%")
            ->select('id', 'name', 'slug')
            ->distinct()
            ->paginate(25);
    }

    public function childCategories()
    {
        return $this->hasMany('App\Category','parent_id', 'id');
    }

    public function parentCategories()
    {
        return $this->hasOne('App\Category','id','parent_id');
    }

    public function posts()
    {
        return $this->belongsToMany('App\Post', 'category_post');
    }
}
