<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tag extends Model
{
    protected $table = 'tags';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name','slug'
    ];

    public $timestamps = false;

    public function posts()
    {
        return $this->belongsToMany('App\Post', 'tag_post');
    }

    public static function getTags($page_size)
    {
        return DB::table('tags')->paginate($page_size);
    }

    public static function getTagsNames()
    {
        return DB::table('tags')->select('name')->get();
    }

    public static function search($term)
    {
        return DB::table('tags')
            ->where('name', 'like', "%". strtolower($term) . "%")
            ->distinct()
            ->paginate(25);
    }

}
