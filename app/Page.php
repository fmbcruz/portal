<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Page extends Model
{
    protected $table = 'pages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'author_id', 'title', 'content', 'excerpt', 'slug', 'status', 'type', 'image', 'parent_id','comment_status','comment_count', 'date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public static function getPages($page_size)
    {
        return DB::table('pages as PAG')
            ->join('users as USR', 'PAG.author_id', '=', 'USR.id')
            ->select('PAG.id', 'PAG.title','USR.name', 'PAG.status','PAG.comment_count', DB::raw(' DATE_FORMAT(PAG.created_at, "%d/%m/%Y %H:%i") as created_at'), DB::raw(' DATE_FORMAT(PAG.date, "%d/%m/%Y %H:%i") as date'), 'PAG.slug')->paginate($page_size);
    }

    public static function search($term)
    {
        return DB::table('pages as PAG')
            ->join('users as USR', 'PAG.author_id', '=', 'USR.id')
            ->where('PAG.title', 'like', "%". strtolower($term) . "%")
            ->orWhere('PAG.excerpt', 'like', "%". strtolower($term) . "%")
            ->orWhere('PAG.content', 'like', "%". strtolower($term) . "%")
            ->select('PAG.id', 'PAG.title','USR.name', 'PAG.status','PAG.comment_count', DB::raw(' DATE_FORMAT(PAG.created_at, "%d/%m/%Y %H:%i") as created_at'), DB::raw(' DATE_FORMAT(PAG.date, "%d/%m/%Y %H:%i") as date'), 'PAG.slug')
            ->distinct()
            ->paginate(25);
    }

    public static function getPageType($type)
    {
        return DB::table('pages')
            ->where('type', '=', $type)
            ->get();

    }

    public function pages()
    {
        return $this->hasMany('App\Page','parent_id');
    }
}
