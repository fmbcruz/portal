<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Statistic extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'city', 'country', 'countryCode', 'isp', 'lat', 'lon', 'org', 'query', 'region', 'regionName', 'status', 'timezone', 'zip', 'url', 'datetime'
    ];

    public $timestamps = null;
}
