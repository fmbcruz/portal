<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Contact extends Model
{
    protected $table = 'contacts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'subject', 'content', 'ip'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];

    public static function getContacts($page_size)
    {
        return DB::table('contacts')
            ->select('id', 'name', 'email', 'subject', 'content', 'ip', DB::raw(' DATE_FORMAT(created_at, "%d/%m/%Y %H:%i") as date'))
            ->orderBy('id', 'DESC')
            ->paginate($page_size);
    }

    public static function search($term)
    {
        return DB::table('contacts')
            ->where('name', 'like', "%". strtolower($term) . "%")
            ->orWhere('email', 'like', "%". strtolower($term) . "%")
            ->orWhere('content', 'like', "%". strtolower($term) . "%")
            ->select('id', 'name', 'email', 'content', 'ip', DB::raw(' DATE_FORMAT(created_at, "%d/%m/%Y %H:%i") as date'))
            ->distinct()
            ->paginate(25);
    }

    public static function getContact($id)
    {
        return DB::table('contacts')
            ->where('id', '=', $id)
            ->select('id', 'name', 'email', 'subject', 'content', 'ip', DB::raw(' DATE_FORMAT(created_at, "%d/%m/%Y %H:%i") as date'))
            ->get();
    }
}
