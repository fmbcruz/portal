<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Post extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'author_id', 'title', 'content', 'excerpt', 'slug', 'status', 'type', 'image', 'parent_id','comment_status','comment_count', 'date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 'deleted_at'
    ];

    protected $dates = ['deleted_at'];

    public static function getPosts($page_size)
    {
        return DB::table('posts as PST')
            ->join('users as USR', 'PST.author_id', '=', 'USR.id')
            ->select('PST.id', 'PST.title','USR.name', 'PST.status','PST.comment_count', DB::raw(' DATE_FORMAT(PST.created_at, "%d/%m/%Y %H:%i") as created_at'), DB::raw(' DATE_FORMAT(PST.date, "%d/%m/%Y %H:%i") as date'), 'PST.slug')
            ->whereNull('PST.deleted_at')
            ->paginate($page_size);
    }

    public static function search($term)
    {
        return DB::table('posts as PST')
            ->join('users as USR', 'PST.author_id', '=', 'USR.id')
            ->where('PST.title', 'like', "%". strtolower($term) . "%")
            ->orWhere('PST.excerpt', 'like', "%". strtolower($term) . "%")
            ->orWhere('PST.content', 'like', "%". strtolower($term) . "%")
            ->select('PST.id', 'PST.title','USR.name', 'PST.status','PST.comment_count', DB::raw(' DATE_FORMAT(PST.created_at, "%d/%m/%Y %H:%i") as created_at'), DB::raw(' DATE_FORMAT(PST.date, "%d/%m/%Y %H:%i") as date'), 'PST.slug')
            ->distinct()
            ->paginate(25);
    }

    public static function searchBlog($term)
    {
        return Post::where('status', '=', 'published')
            ->where('title', 'like', "%". strtolower($term) . "%")
            ->orWhere('excerpt', 'like', "%". strtolower($term) . "%")
            ->orWhere('content', 'like', "%". strtolower($term) . "%")
            ->distinct()
            ->paginate(25);
    }

    public static function getPost($id)
    {
        return DB::table('posts')->where('id', '=', $id)->get();
    }

    public static function deletePost($id)
    {
        return DB::table('posts')->where('id', '=', $id)->delete();
    }

    public static function postPrevAndNext($date)
    {
        $post['prev'] = DB::table('posts')
                        ->where('created_at', '<', $date)
                        ->where('status', '=', 'published')
                        ->whereNull('deleted_at')
                        ->select('title','slug')
                        ->first();

        $post['next'] = DB::table('posts')
                        ->where('created_at', '>', $date)
                        ->where('status', '=', 'published')
                        ->whereNull('deleted_at')
                        ->select('title','slug')
                        ->first();

        return $post;
    }

    public function posts()
    {
        return $this->hasMany('App\Post','parent_id');
    }

    public function author()
    {
        return $this->belongsTo('App\User');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment','post_id');
    }

    public function showComments()
    {
        return $this->comments()->where('approved', '=', 1);
    }

    public function parentComments()
    {
        return $this->comments()->whereNull('comment_id')->where('approved', '=', 1);
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'tag_post');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'category_post');
    }
}
