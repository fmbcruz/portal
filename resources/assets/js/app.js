/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('title-header', require('./components/Title.vue'));
Vue.component('breadcrumbs', require('./components/Breadcrumbs.vue'));
Vue.component('box', require('./components/Box.vue'));
Vue.component('box-header', require('./components/BoxHeader.vue'));
Vue.component('form-head', require('./components/Form.vue'));
Vue.component('panel', require('./components/Panel.vue'));
Vue.component('data-table', require('./components/Table.vue'));
Vue.component('date', require('./components/Date.vue'));
Vue.component('upload', require('./components/Upload.vue'));
Vue.component('template-select', require('./components/Template.vue'));
Vue.component('template-blog', require('./components/TemplateBlog.vue'));
Vue.component('alert', require('./components/Alert.vue'));
Vue.component('experience', require('./components/Experience.vue'));
Vue.component('tag-field', require('./components/TagField.vue'));
Vue.component('menu-drag', require('./components/Menu.vue'));

const app = new Vue({
    el: '#app',
});
