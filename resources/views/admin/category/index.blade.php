@extends('adminlte::page')

@section('title', 'Filipe Cruz - Todas as Páginas')

@section('content_header')
    <title-header title='Categorias'></title-header>
    <breadcrumbs :list="{{ $breadcrumbs }}"></breadcrumbs>
@stop

@php
    function getChild($element)
    {
        static $n = 0;
        $tab = '—';

        foreach($element as $k => $val) {

            if($n > 0) {
                $j = $n;
                while($j > 0) {
                    $tab .= '—';
                    $j--;
                }
            }

            echo '<option value="'. $val->id . '">'. $tab .' ' . $val->name . '</option>';

             if(count($val->childCategories) > 0) {
                $n++;
                return getChild($val->childCategories);
             }
        }

    }
@endphp

@section('content')
    @include('layouts.alert', ['errors', $errors])
    <panel title="Adicionar de categorias" css="success">
        <form-head id="createForm" css="" action="{{ route('categorias.store') }}" method="post" enctype="" token="{{ csrf_token() }}">
            <div class="row">
                <div class="form-group col-md-3">
                    <label for="name">Categoria</label>
                    <input type="text" id="name" name="name" class="form-control" placeholder="Nome da categoria" value="{{ old('name') }}" style=" width: 100%;">
                </div>
                <div class="form-group col-md-3">
                    <label for="slug">Slug</label>
                    <input type="text" id="slug" name="slug" class="form-control" placeholder="{{ URL::to('/') }}/blog/slug-da-categoria" value="{{ old('slug') }}" style=" width: 100%;">
                </div>
                <div class="form-group col-md-3">
                    <label for="slug">Categoria pai</label>
                    <select class="form-control" id="parent_id" name="parent_id">
                            <option value="">Nenhum</option>
                        @foreach($categories as $key => $value)
                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                            @if(count($value->childCategories) > 0)
                               @php
                                    getChild($value->childCategories)
                               @endphp
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <label>&nbsp</label>
                    <button type="submit" class="btn btn-default btn-flat bg-blue btn-block">
                        Adicionar
                    </button>
                </div>
            </div>
        </form-head>
    </panel>

    <panel title="Lista de categorias" css="info">
        <box-header token="{{ csrf_token() }}"></box-header>
        <data-table
                name="tablePages"
                :titles="[['#', ''],['Nome','string'],[ 'Slug','']]"
                :content="{{ json_encode($collection) }}"
                edit="{{ route('categorias') }}"
                exclude="{{ route('categorias') }}"
                token="{{ csrf_token() }}"
        ></data-table>
        <div class="box-footer clearfix">
            <div class="float-right my-2">
                <div class="pull-right">
                    {{ $collection->links() }}
                </div>
            </div>
        </div>
    </panel>

@stop

@section('js')
    <script>
        $(function () {
            $("#name").focusout(function() {
                $("#slug").val($( this ).val())
            });
        });

    </script>
@stop