@extends('adminlte::page')

@section('title', 'Filipe Cruz - Todas as Categorias')

@section('content_header')
    <title-header title='Editar Categoria'></title-header>
    <breadcrumbs :list="{{ $breadcrumbs }}"></breadcrumbs>
@stop

@php
    function getChild($element, $selected)
    {
        static $n = 0;
        $tab = '—';
        $status = '';

        foreach($element as $k => $val) {

            if($n > 0) {
                $j = $n;
                while($j > 0) {
                    $tab .= '—';
                    $j--;
                }
            }

            $val->id == $selected ? $status = 'select' : $status = '';


            echo '<option value="'. $val->id . '" '. $status . '>'. $tab .' ' . $val->name . '</option>';

             if(count($val->childCategories) > 0) {
                $n++;
                return getChild($val->childCategories, $selected);
             }
        }

    }
@endphp

@section('content')
    @include('layouts.alert', ['errors', $errors])
    <form-head id="createForm" css="" action="{{ route('categorias') . '/' . $collection->id }}" method="put" enctype="" token="{{ csrf_token() }}">
        <div class="row">
            <div class="form-group col-md-3">
                <label for="name">Categoria</label>
                <input type="text" id="name" name="name" class="form-control" placeholder="Nome da Categoria" value="{{ $collection->name }}" style=" width: 100%;">
            </div>
            <div class="form-group col-md-3">
                <label for="slug">Slug</label>
                <input type="text" id="slug" name="slug" class="form-control" placeholder="{{ URL::to('/') }}/blog/slug-da-categoria" value="{{ $collection->slug }}" style=" width: 100%;">
            </div>
            <div class="form-group col-md-3">
                <label for="slug">Categoria pai</label>
                <select class="form-control" id="parent_id" name="parent_id">
                    <option value="">Nenhum</option>
                    @foreach($categories as $key => $value)
                        <option value="{{ $value->id }}" {{ $collection->parent_id == $value->id ? 'selected' : '' }}
                        >{{ $value->name }}</option>
                        @if(count($value->childCategories) > 0)
                            @php
                                getChild($value->childCategories, $collection->parent_id)
                            @endphp
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-3">
                <label>&nbsp</label>
                <button type="submit" class="btn btn-default btn-flat bg-blue btn-block">
                    Salvar
                </button>
            </div>
        </div>
    </form-head>

@stop

@section('js')
    <script>
        $(function () {
            $("#name").focusout(function() {
                $("#slug").val($( this ).val())
            });
        })
    </script>
@stop