@extends('adminlte::page')

@section('title', 'Filipe Cruz - Todas as Páginas')

@section('content_header')
    <title-header title='Artigos'></title-header>
    <breadcrumbs :list="{{ $breadcrumbs }}"></breadcrumbs>
@stop

@section('content')
    <panel title="Gerenciamento de artigos" css="info">
        <box-header create="{{ route('criar.artigos') }}" token="{{ csrf_token() }}"></box-header>
        <data-table
                name="tablePages"
                :titles="[['#', ''],['Título','string'],[ 'Autor',''],['Status',''],[ 'Comentários','number'],[ 'Criação',''],[ 'Agendado','icon'], ['Slug', '']]"
                :content="{{ json_encode($collection) }}"
                edit="{{ route('artigos') }}"
                exclude="{{ route('artigos') }}"
                token="{{ csrf_token() }}"
                blog="true"
                urlview="true"
        ></data-table>
        <div class="box-footer clearfix">
            <div class="float-right my-2">
                <div class="pull-right">
                    {{ $collection->links() }}
                </div>
            </div>
        </div>
    </panel>

@stop