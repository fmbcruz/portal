@extends('adminlte::page')

@section('title', 'Filipe Cruz - Todos os Comentários')

@section('content_header')
    <title-header title='Comentários'></title-header>
    <breadcrumbs :list="{{ $breadcrumbs }}"></breadcrumbs>
@stop

@section('content')
    <panel title="Gerenciamento de comentários" css="info">
        <box-header token="{{ csrf_token() }}"></box-header>
        <data-table
                name="tableComments"
                :titles="[['#', ''],['Autor',''], ['Email', ''], ['IP', ''], [ 'Comentário',''], ['Status', ''],['Artigo',''],[ 'Enviado em','']]"
                :content="{{ json_encode($collection) }}"
                edit="{{ route('comentarios') }}"
                exclude="{{ route('comentarios') }}"
                token="{{ csrf_token() }}"
        ></data-table>
        <div class="box-footer clearfix">
            <div class="float-right my-2">
                <div class="pull-right">
                    {{ $collection->links() }}
                </div>
            </div>
        </div>
    </panel>

@stop