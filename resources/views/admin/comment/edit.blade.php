@extends('adminlte::page')

@section('title', 'Filipe Cruz - Todas os Comentários')

@section('content_header')
    <title-header title='Editar comentários'></title-header>
    <breadcrumbs :list="{{ $breadcrumbs }}"></breadcrumbs>
@stop

@section('content')

    <form-head id="createForm" css="" action="{{ $action =  route('comentarios') . '/' . $collection->id }}" method="put" enctype="" token="{{ csrf_token() }}">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="author">Nome</label>
                    <input type="text" id="author" name="author" class="form-control" value="{{ $collection->author }}" style=" width: 100%;">
                </div>
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="text" id="email" name="email" class="form-control" value="{{ $collection->email }}" style=" width: 100%;">
                </div>
                <div class="form-group">
                    <label for="website">Site</label>
                    <input type="text" id="website" name="website" class="form-control" value="{{ $collection->website }}" style=" width: 100%;">
                </div>
                <div class="form-group">
                    <label for="website">Data</label>
                    <input type="text" id="created_at" class="form-control" value="{{ date_format($collection->created_at, 'd/m/Y H:i') }}" style=" width: 100%;" readonly>
                </div>
                <div class="form-group">
                    <label for="approved">Status</label>
                    <select class="form-control" id="approved" name="approved">
                        <option value="0" {{ $collection->approved ? '' : 'selected' }}>Aguardando aprovação</option>
                        <option value="1" {{ $collection->approved ? 'selected' : '' }}>Aprovada</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="content"> Comentário </label>
                    <textarea style=" min-height: 183px; " class="form-control" rows="12" name="content" id="content" >{{ $collection->content }}</textarea>
                </div>
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="author_ip">IP do autor</label>
                        <input type="text" id="author_ip" name="author_ip" class="form-control" value="{{ $collection->author_ip }}" style=" width: 100%;" readonly>
                    </div>
                    <div class="col-md-offset-2 col-md-3">
                        <div class="form-group">
                            <label>&nbsp</label>
                            <a href="{{ route('comentarios') }}">
                                <button type="button" class="btn btn-default btn-flat btn-block">
                                    Voltar
                                </button>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>&nbsp</label>
                            <button type="submit" class="btn btn-default btn-flat btn-block bg-blue">
                                Salvar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form-head>
    @if($collection->childComments->count() > 0)
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-commenting-o"></i>

                <h3 class="box-title">Respostas</h3>
            </div>
            @foreach($collection->childComments as $key => $val)
                <!-- /.box-header -->
                <div class="box-body">
                    <blockquote>
                        <p>{{ $val->content }} </p><small><cite title="Source Title">{{ $val->author }}</cite> ({{ $val->email }}) <br> Em : {{ date_format($collection->created_at, 'd/m/Y H:i') }} - IP: {{ $val->author_ip }}</small>
                    </blockquote>
                </div>
                <!-- /.box-body -->
            @endforeach
        </div>
    @endif

@stop

@section('script')
    <script>

        $(window).on('load', function() {

        });
    </script>
@stop