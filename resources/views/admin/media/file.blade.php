@extends('adminlte::page')

@section('title', 'Filipe Cruz - Todas as Páginas')

@php
    $breadcrumbs = json_encode([
                ['title' => 'Dashboard', 'url' => route('admin'), 'icon' => 'dashboard'],
                ['title' => 'Arquivos de Mídia', 'url' => '', 'icon' => 'files-o'],
            ]);
@endphp

@section('content_header')
    <title-header title='Arquivos' subtitle="Gerencie seus arquivos"></title-header>
    <breadcrumbs :list="{{ $breadcrumbs }}"></breadcrumbs>
@stop

@section('content')

    <iframe src="/uploads?type=file" style="width: 100%; height: 500px; overflow: hidden; border: none;"></iframe>


@stop