@extends('adminlte::page')

@section('title', 'Filipe Cruz - Todos os Usuários')

@section('content_header')
    <title-header title='Usuários'></title-header>
    <breadcrumbs :list="{{ $breadcrumbs }}"></breadcrumbs>
@stop

@section('content')
    <panel title="Gerenciamento de usuários" css="info">
        <box-header create="{{ route('criar.artigos') }}" token="{{ csrf_token() }}"></box-header>
        <data-table
                name="tablePages"
                :titles="[['#', ''],['Nome','string'],[ 'Email',''],['Perfil','']]"
                :content="{{ json_encode($collection) }}"
                edit="{{ route('usuarios') }}"
                exclude="{{ route('usuarios') }}"
                token="{{ csrf_token() }}"
        ></data-table>
        <div class="box-footer clearfix">
            <div class="float-right my-2">
                <div class="pull-right">
                    {{ $collection->links() }}
                </div>
            </div>
        </div>
    </panel>

@stop