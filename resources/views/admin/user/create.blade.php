@extends('adminlte::page')

@section('title', 'Filipe Cruz - Todas os Usuários')

@section('content_header')
    <title-header title='Adicionar novo usuário'></title-header>
    <breadcrumbs :list="{{ $breadcrumbs }}"></breadcrumbs>
@stop

@section('content')


    <form-head id="createForm" css="" action="{{ route('usuarios.store') }}" method="post" enctype="" token="{{ csrf_token() }}">
        <div class="row">
            <panel title="Atributos da página" css="info">
                <div class="form-group">
                    <label for="parent_id">Pai</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-level-up"></i>
                        </div>
                        <select class="form-control input-sm" id="parent_id" name="parent_id">
                            <option value="">(sem pai)</option>
                        </select>
                    </div>
                </div>
                <template-blog></template-blog>
            </panel>
        </div>
    </form-head>

@stop

@section('script')
    <script>

        $(window).on('load', function() {

        });
    </script>
@stop

@section('css')
@stop