@php
$total = 0;
$colors = [1 => 'green', 'light-blue', 'red', 'teal', 'navy', 'purple', 'aqua', 'maroon', 'yellow', 'orange'];
@endphp

@extends('adminlte::page')

@section('title', 'Filipe Cruz - Estatísticas')

@section('content_header')
    <title-header title='Estatísticas'></title-header>
    <breadcrumbs :list="{{ $breadcrumbs }}"></breadcrumbs>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-2">
            <div class="form-group">
                <label>Últimos {{ $days }} dias</label>
                <input type="text" id="days" name="days" placeholder="Número de dias" value="" class="form-control">
            </div>
        </div>
        <div class="form-group col-md-2 no-margin">
            <label>&nbsp;</label>
            <button type="submit" class="btn btn-default btn-flat bg-blue btn-block" id="filterStatistics">
                Filtrar
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            {!! $bar->render() !!}
        </div>
        <div class="col-md-7">
            <panel title="Urls mais visualizados" css="info">
                <table class="table table-bordered">
                    <tbody><tr>
                        <th style="width: 10px">#</th>
                        <th>Url</th>
                        <th class="text-center">Barra</th>
                        <th class="text-center" style="width: 20%;">%</th>
                        <th class="text-center">Total</th>
                    </tr>
                    @foreach($statistics['urls'] as $key => $val)
                        @php($total += $val->total)
                    @endforeach
                    @foreach($statistics['urls'] as $key => $val)
                        @php($color = dechex(rand(0x000000, 0xFFFFFF)))
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $val->label }}</td>
                            <td class="text-center">
                                <div class="progress progress-xs">
                                    <div class="progress-bar" style="width:{{ (int)($val->total*100/$total) }}%;background-color:#{{ $color }};"></div>
                                </div>
                            </td>
                            <td class="text-center"><span class="badge" style="background-color:#{{ $color }};">{{ (int)($val->total*100/$total) }}%</span></td>
                            <td class="text-center">{{ $val->total }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </panel>
        </div>
    </div>

@stop

@section('script')
    <script>
        $(function () {
            $( document ).on('click', '#filterStatistics', function() {
                let url = 'http://filipecruz.com.br/admin/estatisticas/' + $("#days").val();
                window.location.href = url;
            });
        });

    </script>
@stop