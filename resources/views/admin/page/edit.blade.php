@extends('adminlte::page')

@section('title', 'Filipe Cruz - Todas as Páginas')

@section('content_header')
    <title-header title='Editar página'></title-header>
    <breadcrumbs :list="{{ $breadcrumbs }}"></breadcrumbs>
@stop

@section('content')

    @php($action =  route('paginas') . '/' . $collection->id )
    <form-head id="createForm" css="" action="{{ $action }}" method="put" enctype="" token="{{ csrf_token() }}">
        <input type="hidden" id="author_id" name="author_id" value="{{ $collection->author_id }}">
        <div class="row">

            <!-- Template default -->
            @php($template = 'templates.page.' . $collection->type)
            @include($template, array('collection'=>$collection))

            <div class="col-md-3">
                <panel title="Publicar" css="info">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-eye"></i></span>
                            <select class="form-control input-sm" id="status" name="status">
                                @if($collection->status === 'draft')
                                    <option value="draft">Rascunho</option>
                                @else
                                    <option value="published">Publicado</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <select class="form-control input-sm" id="date" name="date">
                                @if($collection->status !== 'schedule')
                                    <option value="" selected>Imediatamente</option>
                                    <option value="schedule">Agendado</option>
                                @else
                                    <option value="">Imediatamente</option>
                                    <option value="schedule" selected>Agendado</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <date datetime="{{ $collection->date }}"></date>
                    <div class="form-group">
                        <button type="submit" class="btn btn-default btn-flat bg-blue">
                            Atualizar
                        </button>
                    </div>
                </panel>
                <panel title="Atributos da página" css="info">
                    <div class="form-group">
                        <label for="parent_id">Pai</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-level-up"></i>
                            </div>
                            <select class="form-control input-sm" id="parent_id" name="parent_id">
                                <option value="">(sem pai)</option>
                                @foreach($pages as $key => $value)
                                    @if($collection->parent_id === $value->id)
                                        <option value="{{ $value->id }}" selected>{{ $value->title }}</option>
                                    @else
                                        <option value="{{ $value->id }}">{{ $value->title }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <template-select template="{{ $collection->type }}"></template-select>
                </panel>
                <panel title="Imagem de Destaque" css="info">
                    <upload id="imagem" name="imagem" image="{{ $collection->image }}"></upload>
                </panel>
            </div>
        </div>
    </form-head>

@stop

@section('script')
    <script>
        $(window).on('load', function() {

            $('#date').change(function () {
                if ($(this).val() == '') {
                    $('#publishDate').hide();
                } else {
                    $('#publishDate').show();
                }
            });
        });
    </script>
@stop