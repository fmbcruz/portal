@extends('adminlte::page')

@section('title', 'Filipe Cruz - Todas as Tags')

@section('content_header')
    <title-header title='Editar tag'></title-header>
    <breadcrumbs :list="{{ $breadcrumbs }}"></breadcrumbs>
@stop

@section('content')
    @include('layouts.alert', ['errors', $errors])
    @php($action =  route('tags') . '/' . $collection->id)
    <form-head id="createForm" css="" action="{{ $action }}" method="put" enctype="" token="{{ csrf_token() }}">
        <div class="row">
            <div class="form-group col-md-3">
                <label for="name">Tag</label>
                <input type="text" id="name" name="name" class="form-control" placeholder="Nome da tag" value="{{ $collection->name }}" style=" width: 100%;">
            </div>
            <div class="form-group col-md-3">
                <label for="slug">Slug</label>
                <input type="text" id="slug" name="slug" class="form-control" placeholder="{{ URL::to('/') }}/blog/slug-da-tag" value="{{ $collection->slug }}" style=" width: 100%;">
            </div>
            <div class="form-group col-md-3">
                <label>&nbsp</label>
                <button type="submit" class="btn btn-default btn-flat bg-blue btn-block">
                    Salvar
                </button>
            </div>
        </div>
    </form-head>

@stop

@section('js')
    <script>
        $(function () {
            $("#name").focusout(function() {
                $("#slug").val($( this ).val())
            });
        })
    </script>
@stop