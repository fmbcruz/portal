@php
    isset($collection['content']) ? $content = json_decode($collection['content']) : $content = null;
@endphp
<div class="col-md-9 blog">
    <div class="form-group">
        <input type="text" id="title" name="title" class="form-control input-lg" placeholder="Digite o título aqui" value="{{ $collection['title'] }}">
    </div>
    <div class="form-group">
        <label class="slug">Slug</label>
        <input type="text" id="slug" name="slug" class="form-control" placeholder="{{ URL::to('/') }}/slug-da-pagina" value="{{ $collection['slug'] }}">
    </div>
   <panel title="Destaque do Blog" css="info">
        <div class="col-md-4 col-md-offset-2">
            <label>Destaque principal</label>
            <select class="form-control" id="highlight_blog_1" name="content[highlight_blog][1]"></select>
        </div>
        <div class="col-md-4">
           <div class="form-group">
               <label>Destaques principais</label>
               <select class="form-control" id="highlight_blog_2" name="content[highlight_blog][2]"></select>
           </div>
           <div class="form-group">
               <select class="form-control" id="highlight_blog_3" name="content[highlight_blog][3]"></select>
           </div>
        </div>
       <div class="col-md-8 col-md-offset-4">
           <div class="form-group">
               <div class="row">
                   <label for="content[page_size]" class="col-sm-3 control-label text-right">Posts por Página</label>
                   <div class="col-sm-3">
                       <input type="text" class="form-control" id="page_size" name="content[page_size]" value="{{ isset($content->page_size) ? $content->page_size : '' }}">
                   </div>
               </div>
           </div>
       </div>
   </panel>

</div>

@section('js')
<script  type="text/javascript">

        let posts = [];
        let select = false;
        @if(!empty($content))
            let high_2 = {{ $content->highlight_blog->{'2'} }};
            let high_3 = {{ $content->highlight_blog->{'3'} }};
        @endif


        function updatePosts(id, array) {
            return $.grep(array, function(e){ return e.id != id; });
        }

        $.ajax({
            url     : '/api/posts/',
            type    : 'GET',
            dataType: 'json',
            success : function(data) {
                posts = data;
                $.each(data, function (i, item) {

                    @if(!empty($content))
                        item.id === {{ $content->highlight_blog->{'1'} }} ? select = true : select = false;
                    @endif

                    $('#highlight_blog_1').append($('<option>', {
                        value   : item.id,
                        text    : item.title,
                        selected: select
                    }));
                });
            },
            error   : function(request, error) {
                alert('Erro: ' + JSON.stringify(request))
            },
            async: false
        });

        if($("#highlight_blog_2").val() === null && high_2 === 'undefined') {
            $( document ).on('change', '#highlight_blog_1', function() {

                let array = updatePosts($( this ).val(), posts);

                $("#highlight_blog_2").empty();

                $.each(array, function (i, item) {
                    @if(!empty($content))
                        item.id === {{ $content->highlight_blog->{'2'} }} ? select = true : select = false;
                    @endif
                    $('#highlight_blog_2').append($('<option>', {
                        value: item.id,
                        text : item.title,
                        selected: select
                    }));
                });
            });
        } else {
            let array = updatePosts({{ $content->highlight_blog->{'1'} }}, posts);
            $.each(array, function (i, item) {
                @if(!empty($content))
                    item.id === {{ $content->highlight_blog->{'2'} }} ? select = true : select = false;
                @endif
                $('#highlight_blog_2').append($('<option>', {
                    value: item.id,
                    text : item.title,
                    selected: select
                }));
            });
        }

        if($("#highlight_blog_3").val() === null && high_3 === 'undefined') {

            $(document).on('change', '#highlight_blog_2', function () {

                let array = updatePosts($(this).val(), posts);
                array = updatePosts($("#highlight_blog_1").val(), array);

                $("#highlight_blog_3").empty();

                $.each(array, function (i, item) {
                    @if(!empty($content))
                        item.id === {{ $content->highlight_blog->{'3'} }} ? select = true : select = false;
                    @endif
                    $('#highlight_blog_3').append($('<option>', {
                        value: item.id,
                        text: item.title,
                        selected: select
                    }));
                });
            });
        } else {
            let array = updatePosts({{ $content->highlight_blog->{'2'} }}, posts);
            array = updatePosts($("#highlight_blog_1").val(), array);

            $("#highlight_blog_3").empty();

            $.each(array, function (i, item) {
                @if(!empty($content))
                    item.id === {{ $content->highlight_blog->{'3'} }} ? select = true : select = false;
                @endif
                $('#highlight_blog_3').append($('<option>', {
                    value: item.id,
                    text: item.title,
                    selected: select
                }));
            });
        }
</script>
@stop

@section('css')
    <style>
        .line {
            height: 6rem;
        }
    </style>
@stop