<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Página não encontrada</title>

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        body {
            background-image: url("/images/404-not-found.gif");
            background-repeat: no-repeat;
            background-position: top;
            overflow: hidden;
        }
        .text-center.h1 {
            font-size: 9.8rem;
            font-weight: bolder;
            text-shadow: 2px 2px #88888887;
        }
        .text-center.h2 {
            margin-top: 255px;
            font-weight: bolder;
        }
        .btn-404 {
            border-radius: 0;
            font-size: 1.2rem;
        }
    </style>
</head>
    <body class="bg-white 404">
        <h1 class="text-center h1">404</h1>
        <h2 class="text-center h2">A página não foi encontrada</h2>
        <div class="form-row text-center">
            <div class="col-12">
                <a href="/">
                    <button class="btn btn-info btn-404">Ir para HOME</button>
                </a>
            </div>
        </div>
    </body>
</html>
