<!doctype html>
<html lang="{{ app()->getLocale() }}">
@extends('layouts.app')

@section('content')
    <div class="div">
    @foreach($collection as $key => $value)
        <div class="row">
            @if($value->image)
            <img src="/images/{{ $value->image }}" class="img-fluid rounded mx-auto d-block">
            @endif
        </div>
        <div class="col-md-12">

            <h1>{{ $value->title }}</h1>
            <h2>{{ $value->excerpt }}</h2>
            {!! $value->content !!}
        </div>
    @endforeach
    </div>

@endsection
