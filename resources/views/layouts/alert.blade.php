@if($errors->all() && !in_array('message', $errors->all()))
    <alert type="warning" icon="warning" title="Atenção!">
        @foreach($errors->all() as $key => $message)
            <slot>{{ $message }}</slot> <br>
        @endforeach
    </alert>
@elseif(in_array('message', $errors->all()))
    @foreach($errors->all() as $key => $message)
        @if($message != 'message')
            <alert type="success alert-fade" icon="check" title="{{ $message }}">
            </alert>
        @endif
    @endforeach
@section('js')
    <script>
        $(function () {
            setTimeout(function(){
                $(".alert-fade").fadeOut(1000);
            }, 5000);
        });
    </script>
@stop
@endif