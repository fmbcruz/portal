@php
    $info['title']   = 'Contato';
    $info['author']  = 'Filipe Cruz';
    $info['excerpt'] = 'Página de contato';
@endphp

@extends('blog.partials.app', ['configs' => $collection, 'info' => (object)$info])

@section('content')
    <!-- s-content
    ================================================== -->
    <section class="s-content s-content--narrow">

        <div class="row">
            @if(!session('status'))
            <div class="s-content__header col-full">
                <h1 class="s-content__header-title">
                    Converse com a gente :)
                </h1>
            </div> <!-- end s-content__header -->
            @endif

            <div class="col-full s-content__main">

                <p class="lead text-center">
                    @if (session('status'))
                        {{ session('status') }}
                    @else
                        Sempre que tiver uma crítica, sugestão ou quiser trocar uma ideia, é só entrar em contato!
                    @endif
                </p>

                @if (!session('status'))
                <form id="contactForm" action="{{ route('contact') }}" method="post" enctype="" class="work-request">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <fieldset>

                        <div class="form-field">
                            <input name="name" type="text" id="name" class="full-width" placeholder="Seu Nome" value="">
                        </div>

                        <div class="form-field">
                            <input name="email" type="text" id="email" class="full-width" placeholder="Seu E-mail" value="" required>
                        </div>

                        <div class="form-field">
                            <input name="subject" type="text" id="subject" class="full-width" placeholder="Assunto" value="">
                        </div>

                        <div class="message form-field">
                            <textarea name="content" id="content" class="full-width" placeholder="Sua Mensagem"></textarea>
                        </div>

                        <button type="submit" class="submit btn btn--primary full-width">Enviar</button>

                    </fieldset>
                </form> <!-- end form -->
                @else
                    <p class="text-center">
                        <i class="fa fa-send" style="font-size: 22rem;"></i>
                    </p>
                @endif

            </div> <!-- end s-content__main -->

        </div> <!-- end row -->

    </section>
@endsection