@php
    $info['title']   = 'Blog de TI';
    $info['author']  = 'Filipe Cruz';
    $info['excerpt'] = 'Blog de estudos de tecnologia da informação como: microinformática, sistemas de computação, comunicação de dados, técnica de programação, engenharia de software e banco de dados.';
@endphp
@extends('blog.partials.app', ['home' => false, 'configs' => $collection, 'info' => (object)$info ])
@section('banner')
<div class="pageheader-content row">
    <div class="col-full">
        <div class="featured">

            <div class="featured__column featured__column--big">
                <div class="entry" style="background-image:url('images/{{$collection->highlights->{1}->image}}');">

                    <div class="entry__content">
                        @foreach($collection->highlights->{1}->categories as $key => $val)
                            <span class="entry__category">
                                <a href="blog/categoria/{{ $val->slug }}">{{ $val->name }}</a>
                            </span>
                        @endforeach
                        <h1>
                            <a href="blog/{{ $collection->highlights->{1}->slug }}" title="">
                                {{ $collection->highlights->{1}->title }}
                            </a>
                        </h1>

                        <div class="entry__info">
                            <a class="entry__profile-pic">
                                <img class="avatar" src="images/{{ $collection->highlights->{1}->author->avatar }}" alt="">
                            </a>

                            <ul class="entry__meta">
                                <li>
                                    <a>
                                        {{ $collection->highlights->{1}->author->name }}
                                    </a>
                                </li>
                                <li>{{ date("d/m/Y h:i", strtotime($collection->highlights->{1}->created_at)) }}</li>
                            </ul>
                        </div>
                    </div> <!-- end entry__content -->

                </div> <!-- end entry -->
            </div> <!-- end featured__big -->

            <div class="featured__column featured__column--small">

                <div class="entry" style="background-image:url('images/{{$collection->highlights->{2}->image}}');">

                    <div class="entry__content">
                        @foreach($collection->highlights->{2}->categories as $key => $val)
                        <span class="entry__category">
                            <a href="blog/categoria/{{ $val->slug }}">{{ $val->name }}</a>
                        </span>
                        @endforeach

                        <h1>
                            <a href="blog/{{ $collection->highlights->{2}->slug }}" title="">
                                {{ $collection->highlights->{2}->title }}
                            </a>
                        </h1>

                        <div class="entry__info">
                            <a class="entry__profile-pic">
                                <img class="avatar" src="images/{{ $collection->highlights->{2}->author->avatar }}" alt="">
                            </a>

                            <ul class="entry__meta">
                                <li><a>{{ $collection->highlights->{2}->author->name }}</a></li>
                                <li>{{ date("d/m/Y h:i", strtotime($collection->highlights->{2}->created_at)) }}</li>
                            </ul>
                        </div>
                    </div> <!-- end entry__content -->

                </div> <!-- end entry -->

                <div class="entry" style="background-image:url('images/{{$collection->highlights->{3}->image}}');">

                    <div class="entry__content">
                        @foreach($collection->highlights->{3}->categories as $key => $val)
                            <span class="entry__category">
                            <a href="blog/categoria/{{ $val->slug }}">{{ $val->name }}</a>
                        </span>
                        @endforeach

                        <h1>
                            <a href="blog/{{ $collection->highlights->{3}->slug }}" title="">
                                {{ $collection->highlights->{3}->title }}
                            </a>
                        </h1>

                        <div class="entry__info">
                            <a class="entry__profile-pic">
                                <img class="avatar" src="images/{{ $collection->highlights->{3}->author->avatar }}" alt="">
                            </a>

                            <ul class="entry__meta">
                                <li><a>{{ $collection->highlights->{3}->author->name }}</a></li>
                                <li>{{ date("d/m/Y h:i", strtotime($collection->highlights->{3}->created_at)) }}</li>
                            </ul>
                        </div>
                    </div> <!-- end entry__content -->

                </div> <!-- end entry -->

            </div> <!-- end featured__small -->
        </div> <!-- end featured -->

    </div> <!-- end col-full -->
</div> <!-- end pageheader-content row -->

@endsection

@section('content')
    @include('blog.partials.blogList', ['collection' => $collection])
    @include('blog.partials.widgetFooter', ['posts' => $collection])
@endsection


