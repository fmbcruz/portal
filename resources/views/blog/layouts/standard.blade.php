@php
    $info['title']   = $collection->title;
    $info['author']  = $collection->author->name;
    $info['excerpt'] = $collection->excerpt;
@endphp

@extends('blog.partials.app', ['configs' => $collection, 'info' => (object)$info])

@php

$months = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

$day = date("j", strtotime($collection->created_at));
$month = date("n", strtotime($collection->created_at));
$year = date("Y", strtotime($collection->created_at));

$created_at = $day . ' de ' . $months[$month] . ' de ' . $year;

$total_comments = $collection->showComments->count();

@endphp

@section('content')
    <!-- s-content
    ================================================== -->
    <section class="s-content s-content--narrow s-content--no-padding-bottom">

        <article class="row format-standard">

            <div class="s-content__header col-full">
                <h1 class="s-content__header-title">
                    {{ $collection->title }}
                </h1>
                <ul class="s-content__header-meta">
                    <li class="date"><i class="fa fa-clock-o"></i> {{ $created_at }}</li> -
                    @if($total_comments > 0)
                        <a href="#comentarios">
                            <li class="comments">
                                <i class="fa fa-comments-o"></i> {{ $total_comments }}
                            </li> -
                        </a>
                    @endif
                    <li class="cat">
                        Em
                        @foreach($collection->categories as $key => $category)
                            <a href="categorias/{{ $category->slug }}">{{ $category->name }}</a>
                        @endforeach
                    </li>
                </ul>
            </div> <!-- end s-content__header -->

            @if(isset($collection->image))
            <div class="s-content__media col-full">
                <div class="s-content__post-thumb">
                    <img src="/images/{{ $collection->image }}"
                         sizes="(max-width: 2000px) 100vw, 2000px" alt="" >
                </div>
            </div> <!-- end s-content__media -->
            @endif
            <div class="col-full s-content__main">

                {!! $collection->content !!}

                @if(count($collection->tags) > 0)
                    <p class="s-content__tags">
                        <span>Post Tags</span>
                        <span class="s-content__tag-list">
                            @foreach($collection->tags as $key => $tag)
                                <a href="tag/{{ $tag->slug }}">{{ $tag->name }}</a>
                            @endforeach
                        </span>
                    </p> <!-- end s-content__tags -->
                @endif

                <div class="s-content__author">
                    <img src="/images/{{ $collection->author->avatar }}" alt="">

                    <div class="s-content__author-about">
                        <h4 class="s-content__author-name">
                            <a href="#0">{{ $collection->author->name }}</a>
                        </h4>

                        <p>{!! $collection->author->description !!}</p>

                        <ul class="s-content__author-social">
                            <li><a href="mailto:{{ $collection->author->email }}?Subject=Contato%20Blog" target="_top">E-mail</a></li>
                            <li><a href="{{ $collection->author->linkedin }}" target="_blank">Linkedin</a></li>
                            <li><a href="{{ $collection->author->twitter }}"  target="_blank">Twitter</a></li>
                        </ul>
                    </div>
                </div>

                <div class="s-content__pagenav">
                    <div class="s-content__nav">
                        @if(!empty($relatedPosts['prev']))
                        <div class="s-content__prev">
                            <a href="/blog/{{ $relatedPosts['prev']->slug }}" rel="prev">
                                <span>Post Anterior</span>
                                {{ $relatedPosts['prev']->title }}
                            </a>
                        </div>
                        @endif
                        @if(!empty($relatedPosts['next']))
                        <div class="s-content__next">
                            <a href="/blog/{{ $relatedPosts['next']->slug }}" rel="next">
                                <span>Próximo Post</span>
                                {{ $relatedPosts['next']->title }}
                            </a>
                        </div>
                        @endif
                    </div>
                </div> <!-- end s-content__pagenav -->

            </div> <!-- end s-content__main -->

        </article>

        @include('blog.partials.comments', ['post_id' => $collection->id, 'comments' => $collection->parentComments, 'total_comments' => $total_comments])
        @include('blog.partials.widgetFooter', ['posts' => $collection])


    </section> <!-- s-content -->

@endsection