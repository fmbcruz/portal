<section class="s-extra">
    <div class="row top">

        <div class="md-six tab-full popular">
            <h3>Posts Populares</h3>

            <div class="block-1-3 block-m-full popular__posts">

                @foreach($posts['popular_posts'] as $key => $post)
                    <article class="col-block popular__post">
                        <a href="#0" class="popular__thumb">
                            <img src="/images/thumbs/{{ $post->image }}" alt="">
                        </a>
                        <h5><a href="/blog/{{ $post->slug }}">{{ $post->title }}</a></h5>
                        <section class="popular__meta">
                            {{--<span class="popular__author"><span>Por</span> <a href="#0"> Author</a></span>--}}
                            <span class="popular__date"><span>Em</span>
                                <time datetime="{{ $post->created_at }}">
                                    {{ date("d/m/Y h:i", strtotime($post->created_at)) }}
                                </time>
                            </span>
                        </section>
                    </article>
                @endforeach
            </div> <!-- end popular_posts -->
        </div> <!-- end popular -->

        <!-- end about -->

    </div> <!-- end row -->

    {{--<div class="row bottom tags-wrap">--}}
        {{--<div class="col-full tags">--}}
            {{--<h3>Tags</h3>--}}

            {{--<div class="tagcloud">--}}
                {{--<a href="#0">Salad</a>--}}
                {{--<a href="#0">Recipe</a>--}}
                {{--<a href="#0">Places</a>--}}
                {{--<a href="#0">Tips</a>--}}
                {{--<a href="#0">Friends</a>--}}
                {{--<a href="#0">Travel</a>--}}
                {{--<a href="#0">Exercise</a>--}}
                {{--<a href="#0">Reading</a>--}}
                {{--<a href="#0">Running</a>--}}
                {{--<a href="#0">Self-Help</a>--}}
                {{--<a href="#0">Vacation</a>--}}
            {{--</div> <!-- end tagcloud -->--}}
        {{--</div> <!-- end tags -->--}}
    {{--</div> <!-- end tags-wrap -->--}}

</section>