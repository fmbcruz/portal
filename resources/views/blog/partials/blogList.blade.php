<!-- s-content
================================================== -->
<section class="s-content">

    @yield('header_content')

    <div class="row masonry-wrap">
        <div class="masonry">

            <div class="grid-sizer"></div>

            @foreach($collection->posts as $key => $post)
                <article class="masonry__brick entry format-standard" data-aos="fade-up">
                    <div class="entry__thumb">
                        <a href="/blog/{{ $post->slug }}" class="entry__thumb-link">
                            <img src="/images/thumbs/{{ $post->image }}" alt="">
                        </a>
                    </div>

                    <div class="entry__text">
                        <div class="entry__header">

                            <div class="entry__date">
                                <a href="/blog/{{ $post->slug }}">{{ date("d/m/Y h:i", strtotime($post->created_at)) }}</a>
                            </div>
                            <h1 class="entry__title"><a href="/blog/{{ $post->slug }}">{{ $post->title }}</a></h1>

                        </div>
                        <div class="entry__excerpt">
                            <p>
                                {!! mb_strimwidth($post->content, 0, 160, '...') !!}
                            </p>
                        </div>
                        <div class="entry__meta">
                            <span class="entry__meta-links">
                                @foreach($post->categories as $val)
                                    <a href="/blog/categoria/{{ $val->slug }}">{{ $val->name }}</a>
                                @endforeach
                            </span>
                        </div>
                    </div>

                </article> <!-- end article -->
            @endforeach

        </div> <!-- end masonry -->
    </div> <!-- end masonry-wrap -->

    <div class="row">
        <div class="col-full">
            {{ $collection->posts->links() }}
        </div>
    </div>

</section> <!-- s-content -->