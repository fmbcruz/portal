<!DOCTYPE html>
<html class="no-js" lang="{{ app()->getLocale() }}">
<head>

    <!--- basic page needs
    ================================================== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $info->title }} | Filipe Cruz</title>
    <meta name="description" content="{{ $info->excerpt }}">
    <meta name="author" content="{{ $info->author }}">

    <!-- mobile specific metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="{{ asset('css/blog/base.css') }}">
    <link rel="stylesheet" href="{{ asset('css/blog/vendor.css') }}">
    <link rel="stylesheet" href="{{ asset('css/blog/main.css') }}">

    <!-- script
    ================================================== -->
    <script src="{{ asset('js/blog/modernizr.js') }}" defer></script>
    <script src="{{ asset('js/blog/pace.min.js') }}" defer></script>
    <script src="{{ asset('js/blog/jquery-3.2.1.min.js') }}"></script>

    <!-- favicons
    ================================================== -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

</head>

<body id="top">

<!-- pageheader
================================================== -->
<section class="s-pageheader {{ isset($home) ? 's-pageheader--home' : '' }}">
    @include('blog.partials.menu', ['menu' => $configs]);
    @yield('banner')
</section>

@yield('content')

<footer class="s-footer">

    <div class="s-footer__bottom">
        <div class="row">
            <div class="col-full">
                <div class="s-footer__copyright">
                    <span>© Copyright Filipe Cruz 2018</span>
                    <span> CÓDIGO É POESIA.</span>
                </div>

                <div class="go-top">
                    <a class="smoothscroll" title="Back to Top" href="#top"></a>
                </div>
            </div>
        </div>
    </div> <!-- end s-footer__bottom -->

</footer> <!-- end s-footer -->


<!-- preloader
================================================== -->
<div id="preloader">
    <div id="loader">
        <div class="line-scale">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
</div>


<!-- Java Script
================================================== -->
<script src="{{ asset('js/blog/plugins.js') }}"></script>
<script src="{{ asset('js/blog/main.js') }}"></script>
