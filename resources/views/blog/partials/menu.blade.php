<header class="header">
    <div class="header__content row">

        <div class="header__logo">
            <a class="logo" href="{{ URL::to('/') }}/blog">
                <h1><b>F</b>ilipe <b>C</b>ruz.</h1>
                <h1 class="mobile"><b>FC</b>.</h1>
            </a>
        </div> <!-- end header__logo -->
        <ul class="header__social">
            @foreach(json_decode($menu['redes_sociais_blog']->value) as $key => $val)
                <li>
                    <a href="{{ $val->url }}" target="_{{ $val->target }}">
                        <i class="fa fa-{{ $val->icon }}" aria-hidden="true"></i>
                    </a>
                </li>
            @endforeach

        </ul> <!-- end header__social -->

        <a class="header__search-trigger" href="#0"></a>

        <div class="header__search">

            <form role="search" method="get" class="header__search-form" action="{{ route('busca') }}">
                <label>
                    <span class="hide-content">Pesquisar por:</span>
                    <input type="search" class="search-field" placeholder="Digite aqui" value="" name="termo" title="Pesquisar por:" autocomplete="off">
                </label>
                <input type="submit" class="search-submit" value="Search">
            </form>

            <a href="#0" title="Sair" class="header__overlay-close">Close</a>

        </div>  <!-- end header__search -->


        <a class="header__toggle-menu" href="#0" title="Menu"><span>Menu</span></a>

        <nav class="header__nav-wrap">

            <h2 class="header__nav-heading h6">Menu do Blog</h2>
            <ul class="header__nav">
            @foreach(json_decode($menu['menu_blog_main']->value) as $key => $menu)
                <li class="{{ Request::path() == $menu->url ? 'current' : '' }} {{ isset($menu->submenu) ? 'has-children' : ''}}">
                    <a href="{{ $menu->url }}" target="{{ $menu->target }}">{{ $menu->label }}</a>
                    @if(isset($menu->submenu))
                        <ul class="sub-menu">
                            @foreach($menu->submenu as $sub => $subMenu)
                                <li>
                                    <a href="{{ $subMenu->url }}" target="{{ $subMenu->target }}">{{ $subMenu->label }}</a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
            </ul> <!-- end header__nav -->

            <a href="#0" title="Close Menu" class="header__overlay-close close-mobile-menu">Close</a>

        </nav> <!-- end header__nav-wrap -->
    </div> <!-- header-content -->
</header> <!-- header -->
