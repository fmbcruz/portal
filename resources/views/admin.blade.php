@extends('adminlte::page')

@section('title', 'Filipe Cruz')

@section('content_header')
    <title-header title='Dashboard' subtitle="Olá, {{ Auth::user()->name }}"></title-header>
    <breadcrumbs :list="{{ $breadcrumbs }}"></breadcrumbs>
@stop

@section('content')

    <div class="row">
        <box size="3" value="{{ $total['posts'] }}" label="Artigos" url="{{ route('artigos') }}" icon="fa fa-newspaper-o" bg="aqua"></box>
        <box size="3" value="{{ $total['pages'] }}" label="Páginas" url="{{ route('paginas') }}" icon="fa fa-files-o" bg="green"></box>
        <box size="3" value="{{ $total['comments'] }}" label="Comentários" url="{{ route('comentarios') }}" icon="fa fa-comments-o" bg="red"></box>
        <box size="3" value="{{ $total['contact'] }}" label="Contatos" url="{{ route('contatos') }}" icon="fa fa-envelope" bg="yellow"></box>
    </div>

    <div class="row">
        <div class="col-md-8">
            <panel title="Visitantes únicos" css="success">
                <div id="map"></div>
            </panel>
        </div>
        <div class="col-md-4">
            <panel title="Posts mais visualizados" css="info">
                <data-table
                        name="tableComments"
                        :titles="[['Post', ''], ['Views', '']]"
                        :content="{{ json_encode($posts) }}"
                ></data-table>
            </panel>
        </div>
    </div>

@stop

@section('script')
    <script>
        function initMap() {

            let map = new google.maps.Map(document.getElementById('map'), {
                zoom: 2,
                center: {lat: 32.858982, lng: 5.317502}
            });
            let infoWin = new google.maps.InfoWindow();
            let markers = locations.map(function(location, i) {
                let marker = new google.maps.Marker({
                    position: location
                });
                google.maps.event.addListener(marker, 'click', function(evt) {
                    infoWin.setContent(location.info);
                    infoWin.open(map, marker);
                });
                return marker;
            });

            // Add a marker clusterer to manage the markers.
            let markerCluster = new MarkerClusterer(map, markers,
                {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

        }
        let locations = [
            @php
                foreach($statistics['visitors'] as $key => $local):
                   $date = new DateTime($local->date);
                   $dateLocal = new DateTime($local->date);
                   $dateLocal = $dateLocal->setTimezone(new DateTimeZone($local->timezone));
                   $local->timezone !== 'America/Sao_Paulo' ? $dateLocal =  ' ('. $dateLocal->format('d/m/Y H:i:s') .')' : $dateLocal = '';
                   $url = explode("/", $local->url);
                   $info = '<p><b>'. $local->org .'</b></p>' .
                            '<p> <b>Local:</b> '. $local->city . ', ' . str_replace('\'','',$local->regionName) . ' - ' . $local->countryCode .'<br>' .
                                '<b>Url:</b> '. end($url) .'<br>' .
                                '<b>Data:</b> '. $date->format('d/m/Y H:i:s') . $dateLocal .'<br>' .
                                '<b>Views:</b> '.$local->total.'</p>';

                    echo "{lat: " . $local->lat . "," . "lng: " . $local->lon . ", info: '" . $info . "'},";
                endforeach;
            @endphp
        ]

    </script>
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCPPRqgkj1hf7XM7IwFxj2Cu015-gPKqFs&callback=initMap">
    </script>
@endsection

@section('css')
    <style>
        #map {
            height: 40rem;
        }
    </style>
@endsection