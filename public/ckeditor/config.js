/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {

    // Config toolbar
    config.toolbar = [{
            name: 'styles',
            items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat', 'Blockquote', '-', 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'Link', 'Unlink', '-', 'Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar'],
        },
        {
            name: 'tools',
            items: ['Styles', 'Format', 'Font', 'FontSize', '-', 'TextColor', 'BGColor', '-', 'Find', 'Replace', 'SelectAll', '-', 'Undo', 'Redo', '-', 'Maximize', 'ShowBlocks', 'Source']
        }];

    // Upload files
    config.filebrowserImageBrowseUrl = '/uploads?type=Images';
    config.filebrowserImageUploadUrl = '/uploads/upload?type=Images&_token=';
    config.filebrowserBrowseUrl = '/uploads?type=Files';
    config.filebrowserUploadUrl = '/uploads/upload?type=Files&_token=';

};

// CKEDITOR.on('dialogDefinition', function (ev) {
//     // Take the dialog name and its definition from the event data.
//     var dialogName = ev.data.name;
//     var dialogDefinition = ev.data.definition;
//
//     // Check if the definition is image dialog window
//     if (dialogName == 'image') {
//         // Get a reference to the "Advanced" tab.
//         var advanced = dialogDefinition.getContents('advanced');
//
//         // Set the default value CSS class
//         var styles = advanced.get('txtGenClass');
//         styles['default'] = 'newsleft';
//     }
// });
